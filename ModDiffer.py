import lxml.html as lh
import glob, os
import numpy as np

all_mods = []
keep_mods = []

# load all mods into list
with open('./all_mods/all_mods.html', 'r') as content_file:
    doc = lh.fromstring(content_file.read())

    # Parse data that are stored between <tr>..</tr> of HTML
    tr_elements = doc.xpath('//td')

    for t in tr_elements:
        if "\n" not in t.text:
            all_mods.append(t.text)


# go through directory of modpacks and load as combined lists
# change to keep_mods
os.chdir("./keep_mods")
for file in glob.glob("*.html"):
    with open(file, 'r') as content_file:
        doc = lh.fromstring(content_file.read())

        # Parse data that are stored between <tr>..</tr> of HTML
        tr_elements = doc.xpath('//td')

        for t in tr_elements:
            if "\n" not in t.text:
                keep_mods.append(t.text)


# remove dublicates from keep_mod list
keep_mods_cleaned = list(dict.fromkeys(keep_mods))

# find mods in all_mods, not present in keep_mods
diff_list = np.setdiff1d(all_mods, keep_mods_cleaned)

for mod in diff_list:
    print(mod)