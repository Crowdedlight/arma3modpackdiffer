# Arma 3 modpack differ

Get html list of all your mods by using the launcher and put it in "all_mods" folder. Name the file ``all_mods.html``.
Then get html lists of the different modpacks you want to keep and put them in the ``keep_mods`` folder. Their names doesn't matter as long as they are ``.html`` files.

Then run the script and you will get a list of mods currently subscribed, but not in any of the keep_mod presets